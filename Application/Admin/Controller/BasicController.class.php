<?php
namespace Admin\Controller;

use Think\Controller;

class BasicController extends Controller {
    function _initialize(){
        !session('?admin_id') && $this->redirect('/login');
        //站点信息
        $sysinfo = getSysInfo();
        $this->assign('sysinfo', $sysinfo);
        //管理员
        $adminInfo = M('SysAdmin')->where(array('id'=>session('admin_id')))->find();
        $this->assign('adminInfo',$adminInfo);
    }
    public function _getNode(){
        //获取spm
        $this->spm = I('get.spm');
        $this->assign('spm', $this->spm);
        //获取节点
        $spm_arr = explode('-',$this->spm);
        $this->nid = end($spm_arr);
        $this->assign('nid', $this->nid);
    }
}
